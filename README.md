# Personal Details Logger #

### Author ###
Amreen Fatima S Surani

### Date ###
September 2015


1. INTRODUCTION
-----------------
My first Android Application that has been coded in Java using Android Studio as the IDE. It was intended to get hands on experience with using intent, allowing users to enter personal details through edit text, spinner, seekbar and displaying the input into the second activity using action listeners. 

2. External Libraries:
-----------------------
None

3. USAGE
---------

The application can used by running any emulator like Genymotion.