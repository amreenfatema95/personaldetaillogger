package com.afssurani.chapter4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class DisplayInput extends AppCompatActivity {

    TextView textView;
    String displayvalue;
    String displayGender;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_input);

        textView = (TextView) findViewById(R.id.Display);
        Intent intent = getIntent();
        Boolean FgenderSelected = intent.getBooleanExtra("FemaleGender", false);
        Boolean MgenderSelected = intent.getBooleanExtra("MaleGender",false);
        if (MgenderSelected == true)
        {
            displayGender = "Male";
        }
        else
        {
            displayGender="Female";
        }
        displayvalue = intent.getStringExtra("Name")+"\n";
        displayvalue = displayvalue + displayGender + "\n";
       displayvalue=displayvalue + intent.getStringExtra("Age")+ "\n";
        displayvalue = displayvalue +intent.getStringExtra("Color");
        textView.setText(displayvalue);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
