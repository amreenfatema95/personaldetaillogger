package com.afssurani.chapter4;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    Button nextButton;
    EditText name;
    RadioButton Fgender;
    RadioButton Mgender;
    String colour;
    TextView ageView;
    SeekBar ageBar;
    int value = 0;
    Spinner colorSpinner;
    TextView colorText;




    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nextButton = (Button) findViewById(R.id.nextButton);
        name = (EditText) findViewById(R.id.user_name);
        Fgender =(RadioButton) findViewById(R.id.radioButton);
        Mgender = (RadioButton) findViewById(R.id.radioButton2);
        colorSpinner = (Spinner) findViewById(R.id.colorList);
        ageView = (TextView) findViewById(R.id.age_TextView);
        ageBar = (SeekBar) findViewById(R.id.AgeSeekBar);

        ageBar.setMax(102);

        displayValue();


        ageBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value = progress;
                displayValue();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,R.array.colorList,android.R.layout.simple_spinner_item);
        colorSpinner.setAdapter(adapter);
        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                colorText = (TextView) view;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


    }



    public void displayValue()
    {
        ageView.setText(Integer.toString(value));
        ageBar.setProgress(value);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextScreen(View view)
    {
        Intent intent = new Intent(MainActivity.this,DisplayInput.class);
        intent.putExtra("Name",name.getText().toString());
        intent.putExtra("FemaleGender", Fgender.isChecked());
        intent.putExtra("MaleGender",Mgender.isChecked());
        intent.putExtra("Age",ageView.getText().toString());
        intent.putExtra("Color",colorText.getText().toString());
        startActivity(intent);

    }


}
